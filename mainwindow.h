//Written by Daniel Arroyo <danyorocks@gmail.com>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define ARRAY 30

#include <QtGui/QMainWindow>
#include <QtGui/QDialog>
#include "acercadialogo.h"

class QAction;
class QGridLayout;
class QKeyEvent;
class QLabel;
class QSpacerItem;
class QPushButton;
class QTime;
class QTimer;
class QVBoxLayout;
class QWidget;
class QStatusBar;
class QGroupBox;
class QMenuBar;
class QMenu;
class AcercaDialogo;

class MainWindow : public QMainWindow
{
  Q_OBJECT
    public:
  MainWindow(QWidget *parent = 0);
  private slots:
  void iniciarDino();
  void colores();
  void iniciarBasicas();
  void randomBasicas();
  void cargarNivel(int cif);
  void nivelTerminado();
  void aciertoFalla(QString res, QString cor);
  void print_N(QString n1);
  void backSpace();
  void cleanEdit();
  void print_0();
  void print_1();
  void print_2();
  void print_3();
  void print_4();
  void print_5();
  void print_6();
  void print_7();
  void print_8();
  void print_9();
  void nivel1();
  void nivel2();
  void nivel3();
  void nivel4();
  void nivel5();
  void nivel6();
  void nivel7();
  void nivel8();
  void nivel9();
  void crearEstadoB();
  void respuestaTiempo();
  void ocultarDigitos();
  void crearAcciones();
  void crearMenu();
  void inicio();
  void acerca();
  void extraer();
 protected:
  void keyPressEvent(QKeyEvent *event);
 private:
  QWidget *dinosumaWidget;
  QStatusBar *estadoBarra;
  QLabel *tiempoEBLabel;
  QLabel *fechaEBLabel;
  QLabel *horaEBLabel;
  QVBoxLayout *mainVLayout;
  QGroupBox *labelsGBox;
  QLabel *infoLabel;
  QGridLayout *gridSLayout;
  QLabel *xLabel[ARRAY];
  QGridLayout *gridTLayout;
  QSpacerItem *hLeftSpacer;
  QLabel *typeLabel;
  QLabel *respLabel;
  QSpacerItem *hRightSpacer;
  QGroupBox *typerespGBox;
  QLabel *failLabel;
  QGridLayout *gridBLayout;
  QPushButton *xButton[10];
  QPushButton *backButton;
  QPushButton *cleanButton;
  QPushButton *ecuaciones1Button;
  QPushButton *ecuaciones2Button;
  QPushButton *optionsButton;
  QPushButton *estadisButton;
  QPushButton *desigualButton;
  QPushButton *helpButton;
  QPushButton *potenciaButton;
  QPushButton *quitButton;
  QTimer *respuestaTimer;
  QTimer *preparacionTimer;
  QTimer *ocultarTimer;
  QKeyEvent *KeyEvent;
  QMenuBar *menuBarra;
  QMenu *menu_Iniciar;
  QMenu *menu_Ecuaciones1;
  QAction *act_e1Nivel1;
  QAction *act_e1Nivel2;
  QAction *act_e1Nivel3;
  QAction *act_e1Nivel4;
  QAction *act_e1Nivel5;
  QAction *act_e1Nivel6;
  QAction *act_e1Nivel7;
  QAction *act_e1Nivel8;
  QAction *act_e1Nivel9;
  QMenu *menu_Desigualdades;
  QAction *act_dNivel1;
  QAction *act_dNivel2;
  QAction *act_dNivel3;
  QAction *act_dNivel4;
  QAction *act_dNivel5;
  QAction *act_dNivel6;
  QAction *act_dNivel7;
  QAction *act_dNivel8;
  QAction *act_dNivel9;
  QAction *act_Guardar;
  QAction *act_guardarSalir;
  QAction *act_Salir; 
  QMenu *menu_Estadisticas;
  QAction *act_Resumen;
  QMenu *menu_Opciones;
  QAction *act_tamanoFuente;
  QAction *act_tamanoFuenteSigno;  
  QMenu *menu_Ayuda;
  QAction *act_Acerca;
  QString str_correct, str_fail, str_numero, str_result, s[ARRAY],
    str_restantes, str_percent, str_intentos, str_type, str_d,
    color[ARRAY], str_signo, str_s, str_categoria, 
    str_tjuego, str_trespuesta, str_tpreparacion, str_nombre, 
    str_sep, str_inicio, str_variable;
  int c, o, i, n[ARRAY], x, y, z, t, r, N;
};
#endif