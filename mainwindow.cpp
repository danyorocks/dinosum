//Written by Daniel Arroyo <danyorocks@gmail.com>

#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QSpacerItem>
#include <QtGui/QLabel>
#include <QtGui/QGroupBox>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include <QKeyEvent>
#include <QTimer>
#include <QTime>
#include <QMenuBar>
#include <QMenu>
#include <QtGui/QDialog>

#include "mainwindow.h"
#include "acercadialogo.h"

#define MAXN 30
#define CERO "0"
#define DIEZ "10"
#define CIEN "100"
#define MILL "1000"
#define SUMA "+"
#define REST "-"
#define MULT "·"
#define DIVI "/"
#define IGUA "="
#define MIZQ ">"
#define MDER "<"
#define PIZQ "("
#define PDER ")"
#define TIDP 3
#define TIDM 1
#define INTE 10

int correct = 0, fail = 0, seguir = 0, intentos = 0, slen = 0, 
  restantes = 0, cifras = 0, d = 0, digitos = 0,
  lsuper = 0, linfer = 0, igual = 0, variable = 0, tocultar = 0,
  trespuesta = 0, tpreparacion = 0, inicio = 0, nivel = 0, 
  nterminado = 0;
float percent;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
  str_variable = "y";
  respuestaTimer = new QTimer(this);
  preparacionTimer = new QTimer(this);
  ocultarTimer = new QTimer(this);
  this->setWindowIcon(QIcon("imagenes/dino.png")); 
  dinosumaWidget = new QWidget(this);
  mainVLayout = new QVBoxLayout(dinosumaWidget);
  this->setCentralWidget(dinosumaWidget);
  menuBarra = new QMenuBar(this);
  this->setMenuBar(menuBarra);
  menu_Iniciar = new QMenu();
  menu_Iniciar->setTitle("&Iniciar");
  menu_Ecuaciones1 = new QMenu();
  menu_Ecuaciones1->setTitle("&Ecuaciones");
  menu_Desigualdades = new QMenu();
  menu_Desigualdades->setTitle("&Desigualdades");
  menu_Estadisticas = new QMenu();
  menu_Estadisticas->setTitle("E&stadisticas");
  menu_Opciones = new QMenu();
  menu_Opciones->setTitle("&Opciones");
  menu_Ayuda = new QMenu();
  menu_Ayuda->setTitle("&Ayuda");
  estadoBarra = new QStatusBar(this);
  this->setStatusBar(estadoBarra);
  infoLabel = new QLabel();
  mainVLayout->addWidget(infoLabel);
  labelsGBox = new QGroupBox(this); 
  gridSLayout = new QGridLayout(labelsGBox);
  xLabel[1] = new QLabel();
  xLabel[2] = new QLabel();
  xLabel[3] = new QLabel();
  xLabel[4] = new QLabel();
  xLabel[5] = new QLabel();
  xLabel[6] = new QLabel();
  xLabel[7] = new QLabel();
  xLabel[8] = new QLabel();
  xLabel[9] = new QLabel();
  xLabel[10] = new QLabel();
  xLabel[11] = new QLabel();
  xLabel[12] = new QLabel();
  xLabel[13] = new QLabel();
  xLabel[14] = new QLabel();
  xLabel[15] = new QLabel();
  xLabel[16] = new QLabel();
  xLabel[17] = new QLabel();
  xLabel[18] = new QLabel();
  xLabel[19] = new QLabel();
  xLabel[20] = new QLabel();
  xLabel[21] = new QLabel();
  xLabel[22] = new QLabel();
  xLabel[23] = new QLabel();
  xLabel[24] = new QLabel();
  xLabel[25] = new QLabel();
  xLabel[26] = new QLabel();
  xLabel[27] = new QLabel();
  xLabel[28] = new QLabel();
  xLabel[29] = new QLabel();
  gridSLayout->addWidget(xLabel[1], 0, 0, 1, 1);
  gridSLayout->addWidget(xLabel[2], 0, 1, 1, 1);
  gridSLayout->addWidget(xLabel[3], 0, 2, 1, 1);
  gridSLayout->addWidget(xLabel[4], 0, 3, 1, 1);
  gridSLayout->addWidget(xLabel[5], 0, 4, 1, 1);
  gridSLayout->addWidget(xLabel[6], 0, 5, 1, 1);
  gridSLayout->addWidget(xLabel[7], 0, 6, 1, 1);
  gridSLayout->addWidget(xLabel[8], 0, 7, 1, 1);
  gridSLayout->addWidget(xLabel[9], 0, 8, 1, 1);
  gridSLayout->addWidget(xLabel[10], 0, 9, 1, 1);
  gridSLayout->addWidget(xLabel[11], 0, 10, 1, 1);
  gridSLayout->addWidget(xLabel[12], 0, 11, 1, 1);
  gridSLayout->addWidget(xLabel[13], 0, 12, 1, 1);
  gridSLayout->addWidget(xLabel[14], 0, 13, 1, 1);
  gridSLayout->addWidget(xLabel[15], 0, 14, 1, 1);
  gridSLayout->addWidget(xLabel[16], 0, 15, 1, 1);
  gridSLayout->addWidget(xLabel[17], 0, 16, 1, 1);
  gridSLayout->addWidget(xLabel[18], 0, 17, 1, 1);
  gridSLayout->addWidget(xLabel[19], 0, 18, 1, 1);
  gridSLayout->addWidget(xLabel[20], 0, 19, 1, 1);
  gridSLayout->addWidget(xLabel[21], 0, 20, 1, 1);
  gridSLayout->addWidget(xLabel[22], 0, 21, 1, 1);
  gridSLayout->addWidget(xLabel[23], 0, 22, 1, 1);
  gridSLayout->addWidget(xLabel[24], 0, 23, 1, 1);
  gridSLayout->addWidget(xLabel[25], 0, 24, 1, 1);
  gridSLayout->addWidget(xLabel[26], 0, 25, 1, 1);
  gridSLayout->addWidget(xLabel[27], 0, 26, 1, 1);
  gridSLayout->addWidget(xLabel[28], 0, 27, 1, 1);
  gridSLayout->addWidget(xLabel[29], 0, 28, 1, 1);
  mainVLayout->addWidget(labelsGBox);
  typerespGBox = new QGroupBox(this); 
  gridTLayout = new QGridLayout(typerespGBox);
  hRightSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  typeLabel = new QLabel();
  respLabel = new QLabel();
  hLeftSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  gridTLayout->addItem(hLeftSpacer, 0, 0, 1, 1);
  gridTLayout->addWidget(typeLabel, 0, 1, 1, 1);
  gridTLayout->addWidget(respLabel, 0, 2, 1, 1);
  gridTLayout->addItem(hRightSpacer, 0, 3, 1, 1);
  mainVLayout->addWidget(typerespGBox);
  failLabel = new QLabel();
  mainVLayout->addWidget(failLabel);
  gridBLayout = new QGridLayout();
  xButton[0] = new QPushButton(); 
  xButton[1] = new QPushButton(); 
  xButton[2] = new QPushButton(); 
  xButton[3] = new QPushButton();
  xButton[4] = new QPushButton();
  xButton[5] = new QPushButton();
  xButton[6] = new QPushButton();
  xButton[7] = new QPushButton();
  xButton[8] = new QPushButton();
  xButton[9] = new QPushButton();
  backButton = new QPushButton();
  cleanButton = new QPushButton();
  ecuaciones1Button = new QPushButton();
  ecuaciones2Button = new QPushButton();
  estadisButton = new QPushButton();
  optionsButton = new QPushButton();
  desigualButton = new QPushButton();
  helpButton = new QPushButton();
  potenciaButton = new QPushButton();
  quitButton = new QPushButton();
  gridBLayout->addWidget(xButton[7], 0, 0, 1, 1);
  gridBLayout->addWidget(xButton[8], 0, 1, 1, 1);
  gridBLayout->addWidget(xButton[9], 0, 2, 1, 1);
  gridBLayout->addWidget(ecuaciones1Button, 0, 3, 1, 1);
  gridBLayout->addWidget(estadisButton, 0, 4, 1, 1);
  gridBLayout->addWidget(xButton[4], 1, 0, 1, 1);
  gridBLayout->addWidget(xButton[5], 1, 1, 1, 1);
  gridBLayout->addWidget(xButton[6], 1, 2, 1, 1);
  gridBLayout->addWidget(ecuaciones2Button, 1, 3, 1, 1);
  gridBLayout->addWidget(optionsButton, 1, 4, 1, 1);
  gridBLayout->addWidget(xButton[1], 2, 0, 1, 1);
  gridBLayout->addWidget(xButton[2], 2, 1, 1, 1);
  gridBLayout->addWidget(xButton[3], 2, 2, 1, 1);
  gridBLayout->addWidget(desigualButton, 2, 3, 1, 1);
  gridBLayout->addWidget(helpButton, 2, 4, 1, 1);
  gridBLayout->addWidget(xButton[0], 3, 0, 1, 1);
  gridBLayout->addWidget(backButton, 3, 1, 1, 1);
  gridBLayout->addWidget(cleanButton, 3, 2, 1, 1);
  gridBLayout->addWidget(potenciaButton, 3, 3, 1, 1);
  gridBLayout->addWidget(quitButton, 3, 4, 1, 1);
  mainVLayout->addLayout(gridBLayout);
  iniciarDino();  
  connect(ecuaciones1Button, SIGNAL(clicked()), this, SLOT(iniciarBasicas()));
  connect(respuestaTimer, SIGNAL(timeout()), this, SLOT(respuestaTiempo()));  
  connect(preparacionTimer, SIGNAL(timeout()), this, SLOT(randomBasicas()));
  connect(ocultarTimer, SIGNAL(timeout()), this, SLOT(ocultarDigitos()));
}

void MainWindow::iniciarBasicas()
{
  if(seguir == 1) {
      str_type = typeLabel->text();
      aciertoFalla(str_type, str_result);
      respuestaTimer->stop();
      seguir = 0;
      r = 0;    
      return; }
  ecuaciones1Button->setEnabled(false);
  tpreparacion = TIDP;
  typeLabel->setStyleSheet("");
  typeLabel->text() = "";
  typeLabel->setText("");
  str_type = "";
  respLabel->setStyleSheet("");
  respLabel->text() = "";
  respLabel->setText("");
  str_result = "";
  str_s = "";
  if(nivel == 0) {
    typeLabel->setText("<font color=blue>Selecciona un nivel</font>");
    estadoBarra->showMessage(tr("Para iniciar primero selecciona un nivel.")); 
    ecuaciones1Button->setEnabled(true); return; }
  if(nivel != 0 && nterminado == 1) { cargarNivel(cifras); nterminado = 0; }
  if(restantes == 0) { nivelTerminado(); return; };
  xLabel[variable]->setText("<font color=white>" + str_variable + "</font>");
  typeLabel->setText("<font color=green>DINOSAURIOS</font>");
  typerespGBox->setStyleSheet("background-color:yellow;");
  d = 0;
  for(i = 1; i < cifras; i++)
    {
      if(n[i] != 0) 
	{
	  d++;
	  str_d = QString::number(d);
	  xLabel[i]->setText
	    ("n<font><sub>" + str_d + "</sub></font>");
	}
    }
  preparacionTimer->start(1000);
}

void MainWindow::randomBasicas()
{
  srand(QTime(0, 0, 0).secsTo(QTime::currentTime()));
  if(tpreparacion > 0) {
    tpreparacion--;
    trespuesta = 0;
    str_trespuesta = "0";
    str_tpreparacion = QString::number(tpreparacion);
    typeLabel->setText("<font color=green>" + str_tpreparacion + "</font>");
    failLabel->setText("Restantes = " + str_restantes + str_sep + "Aciertos = " + str_correct + str_sep + "Fallas = " + str_fail  + str_sep + "Porciento = " + str_percent + " [%]" + str_sep + "Tiempo = " + str_trespuesta + " [s]");
    estadoBarra->showMessage(tr("Preparate...")); }
  if(tpreparacion == 0)
    {
      typerespGBox->setStyleSheet("");
      for(i = 1; i < cifras; i++) 
	{
	  str_s = s[i];
	  if(str_s != SUMA && str_s != REST && str_s != MULT && 
	     str_s != DIVI && str_s != str_variable && 
	     str_s != MIZQ && str_s != MDER &&
	     str_s != IGUA)
	    {
	      bool ok;
	      digitos = str_s.toInt(&ok);
	      linfer = digitos / 10;
	      lsuper = digitos - 1;
	      n[i] = rand() % digitos;
	      do {
		n[i] = rand() % digitos;
	      } while(n[i] < linfer + 3 || n[i] > lsuper);
	      str_numero = QString::number(n[i]);
	      int nc = rand() % 29;
	      do { nc = rand() % 29; } while(nc == 0);
	      xLabel[i]->setStyleSheet("background-color:"+ color[nc] +";");
	      xLabel[i]->setText(str_numero);
	    }
	}
      if(nivel == 1) r = n[2] * n[4] + n[6] + n[8];
      if(nivel == 2) r = n[2] * n[4] + n[6];
      if(nivel == 3) r = n[2] + n[4] + n[6] + n[8]; 
      if(nivel == 4) r = n[2] * n[4] * n[6] + n[8];
      if(nivel == 5) r = n[2] * n[4] + n[6] * n[8] + n[10];
      if(nivel == 6) r = n[2] + n[4] + n[6] + n[8] + n[10]; 
      if(nivel == 7) r = n[2] + n[4] + n[6] + n[8] + n[10];
      if(nivel == 8) r = n[2] + n[4] + n[6] + n[8] + n[10] + n[12];
      if(nivel == 9) r = n[2] + n[4] + n[6] + n[8] + n[10] + n[12] + n[14];     
      str_result = QString::number(r);
      str_tpreparacion = "";	
      preparacionTimer->stop();
      typeLabel->setText("");
      estadoBarra->showMessage(tr("Escribe tu respuesta."));
      ocultarTimer->start(1000);
    }
}

void MainWindow::ocultarDigitos()
{
  if(tocultar == TIDM)
    {
      d = 0;
      for(i = 1; i < cifras; i++)
	{
	  if(n[i] != 0) 
	    {
	      d++;
	      str_d = QString::number(d);
	      xLabel[i]->setText("n<font><sub>" + str_d + "</sub></font>");
	    }
	}
      seguir = 1;
      ocultarTimer->stop();
      tocultar = 0;
      respuestaTimer->start(1000);
    }
  tocultar++;
  ecuaciones1Button->setEnabled(true);
}

void MainWindow::aciertoFalla(QString res, QString cor)
{
  for(i = 1; i < cifras; i++) {
    if(n[i] != 0) {
      str_d = QString::number(n[i]);
      xLabel[i]->setText(str_d); } }
  restantes--;
  str_restantes = QString::number(restantes);
  if(res == cor) {
    correct++;
    str_correct = QString::number(correct);
    percent = (correct * 100) / intentos;
    str_percent = QString::number(percent);
    failLabel->setText("Restantes = " + str_restantes + str_sep + "Aciertos = " + str_correct + str_sep + "Fallas = " + str_fail  + str_sep + "Porciento = " + str_percent + " [%]" + str_sep + "Tiempo = " + str_trespuesta + " [s]");
    xLabel[variable]->setText("<font color=white>" + str_result + "</font>");
    typeLabel->setText("<font color=white>" + typeLabel->text() + "<font color=firebrick> = </font>" + str_variable + "</font>");
    typeLabel->setStyleSheet("background-color: rgb(135, 206, 235);");
    respLabel->setStyleSheet("");
    str_result = "";
    typeLabel->text() = "";
    estadoBarra->showMessage(tr("Respuesta correcta, bien."));
  } else {
    fail++;
    str_fail = QString::number(fail);
    percent = (correct * 100) / intentos;
    str_percent = QString::number(percent);
    failLabel->setText("Restantes = " + str_restantes + str_sep +"Aciertos = " + str_correct + str_sep + "Fallas = " + str_fail + str_sep + "Porciento = " + str_percent  + " [%]" + str_sep + "Tiempo = " + str_trespuesta + " [s]");
    xLabel[variable]->setText("<font color=white>" + str_result + "</font>");
    if(typeLabel->text() == "") typeLabel->setText("<font color=crimson>muy mal </font>"); else
      typeLabel->setText(typeLabel->text() + QString::fromUtf8("<font color=firebrick> \342\211\240 </font>") + str_variable);
    respLabel->setText("; <font color=white>" + str_result + "</font>" + "<font color=firebrick> = </font>" + "<font color=white>" + str_variable + "</font>");
    respLabel->setStyleSheet("background-color: rgb(135, 206, 235);");
    typeLabel->setStyleSheet("");
    str_result = "";
    typeLabel->text() = "";
    estadoBarra->showMessage(tr("Respuesta incorrecta.")); }
}

void MainWindow::backSpace()
{
  if(seguir == 1) {
    slen = str_type.length();
    str_type = str_type.left(slen - 1);
    ecuaciones1Button->setFocus();
    typeLabel->setText(str_type); }
}

void MainWindow::print_N(QString n)
{
  if(seguir == 1) {
    typeLabel->setText(typeLabel->text() + n);
    ecuaciones1Button->setFocus();
    str_type = str_type + n; }
}

void MainWindow::cleanEdit()
{
  if(seguir == 1) {
    typeLabel->text() = ""; 
    typeLabel->setText("");
    ecuaciones1Button->setFocus();
    str_type = ""; }
}

void MainWindow::inicio()
{
  QFont fonttype;
  fonttype.setPointSize(34);
  QFont fontnumero;
  fontnumero.setPointSize(34);
  str_inicio = "DINOSUMA";
  for(i = 1; i < str_inicio.length() + 1; i++)
    {
      xLabel[i]->setText(str_inicio.mid(i - 1, 1));
      xLabel[i]->setStyleSheet("color:#ff00ff;");
      xLabel[i]->setFont(fontnumero);
    }
  typeLabel->setFont(fonttype);
  for(i = str_inicio.length() + 1; i < 30; i++) xLabel[i]->hide();
}

void MainWindow::cargarNivel(int cif)
{
  QFont fonttype;
  fonttype.setPointSize(34);
  QFont fontresp;
  fontresp.setPointSize(34);
  QFont fontnumero;
  fontnumero.setPointSize(34);
  QFont fontsigno;
  fontsigno.setPointSize(32);
  restantes = intentos;
  d = 0;
  srand(QTime(0, 0, 0).secsTo(QTime::currentTime()));
  for(i = 1; i < cif; i++)
    {
      if(s[i] == DIEZ || s[i] == CIEN || s[i] == MILL) {
	d++;
	str_d = QString::number(d);
	xLabel[i]->setText("n<font><sub>" + str_d + "</sub></font>");
	int nc = rand() % 17;
	//do { nc = rand() % 17; } while(nc == 0);
	xLabel[i]->setStyleSheet("background-color:"+ color[nc]);
	xLabel[i]->setFont(fontnumero);
	bool ok;
	N = s[i].toInt(&ok);
	n[i] = N; }
      if(s[i] == SUMA || s[i] == REST || 
	 s[i] == MULT || s[i] == DIVI) {
	xLabel[i]->setText("<font color=olive>" + s[i] + "</font>");
	xLabel[i]->setFont(fontsigno);
	n[i] = 0; }
      if(s[i] == str_variable) {
	xLabel[i]->setText("<font color=#ffffff>" + s[i] + "</font>");
	xLabel[i]->setStyleSheet("background-color:#87ceeb;");	  
	xLabel[i]->setFont(fontnumero); }
      if(s[i] == MIZQ || s[i] == MDER || 
	 s[i] == IGUA) {
	xLabel[i]->setText("<font color=firebrick>" + s[i] + "</font>");
	xLabel[i]->setFont(fontnumero);
	n[i] = 0; }
    }
  str_correct= "0";
  str_fail = "0";
  str_percent = "0";
  str_intentos = QString::number(intentos);
  str_restantes = QString::number(restantes);
  str_trespuesta = QString::number(trespuesta);
  infoLabel->setText("Nivel = <font color=blue>" + str_nombre +"</font>" + str_sep + "Intentos = " + str_intentos + str_sep + "Categoria = <font color=blue>" + str_categoria + "</font>");
  failLabel->setText("Restantes = " + str_restantes + str_sep +"Aciertos = " + str_correct + str_sep + "Fallas = " + str_fail + str_sep + "Porciento = " + str_percent  + " [%]" + str_sep + "Tiempo = " + str_trespuesta + " [s]");
  typeLabel->setFont(fonttype);
  respLabel->setFont(fontresp);
  r = 0;
}

void MainWindow::nivel1()
{
  nivel = 1;
  intentos = INTE;
  str_nombre = "<font color=blue>1 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = MULT; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ;
  s[9] = IGUA; s[10] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel2()
{
  nivel = 2;
  intentos = INTE;
  str_nombre = "<font color=blue>2 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = CIEN; s[3] = MULT; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = CIEN; s[7] = IGUA; s[8] = str_variable; 
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel3()
{
  nivel = 3;
  intentos = INTE;
  str_nombre = "<font color=blue>3 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = CIEN; s[3] = SUMA; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = SUMA; s[8] = CIEN; 
  s[9] = IGUA; s[10] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel4()
{
  nivel = 4;
  intentos = INTE;
  str_nombre = "<font color=blue>4 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = MULT; s[4] = DIEZ; 
  s[5] = MULT; s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ; 
  s[9] = IGUA; s[10] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel5()
{
  nivel = 5;
  intentos = INTE;
  str_nombre = "<font color=blue>5 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = MULT; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = MULT; s[8] = DIEZ; 
  s[9] = SUMA; s[10] = CIEN; s[11] = IGUA; s[12] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel6()
{
  nivel = 6;
  intentos = INTE;
  str_nombre = "<font color=blue>6 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = CIEN; s[3] = SUMA; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ; 
  s[9] = SUMA; s[10] = CIEN; s[11] = IGUA; s[12] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel7()
{
  nivel = 7;
  intentos = INTE;
  str_nombre = "<font color=blue>7 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = SUMA; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ; 
  s[9] = SUMA; s[10] = DIEZ; s[11] = IGUA; s[12] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel8()
{
  nivel = 8;
  intentos = INTE;
  str_nombre = "<font color=blue>8 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = SUMA; s[4] = DIEZ; s[5] = SUMA; 
  s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ; s[9] = SUMA; 
  s[10] = DIEZ; s[11] = SUMA; s[12] = DIEZ; 
  s[13] = IGUA; s[14] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::nivel9()
{
  nivel = 9;
  intentos = INTE;
  str_nombre = "<font color=blue>8 Ecuacion de 1er grado</font>";
  str_categoria = "<font color=green>ALGEBRA</font>";
  for(i = 1; i < 30; i++) { s[i] = CERO; n[i] = 0; }
  s[1] = SUMA; s[2] = DIEZ; s[3] = SUMA; s[4] = DIEZ; 
  s[5] = SUMA; s[6] = DIEZ; s[7] = SUMA; s[8] = DIEZ; 
  s[9] = SUMA; s[10] = DIEZ;  s[11] = SUMA; s[12] = DIEZ; 
  s[13] = SUMA; s[14] = DIEZ; s[15] = IGUA;  s[16] = str_variable;
  extraer();
  cargarNivel(cifras);
}

void MainWindow::extraer()
{
  for(i = 1; i < 30; i++)
    {
      if(s[i] == str_variable) variable = i;
      if(s[i] == IGUA) igual = i;
      if(s[i] == CERO) { cifras = i; break; }
    }
  for(i = 1; i < cifras; i++) xLabel[i]->show();
  for(i = cifras; i < 30; i++) xLabel[i]->hide();
  typeLabel->setText("<font color=blue>DINOSAURIOS</font>");
  typerespGBox->setStyleSheet("background-color:skyblue;");
}

void MainWindow::nivelTerminado()
{
  respuestaTimer->stop();
  preparacionTimer->stop();
  ocultarTimer->stop();
  if(percent == 100) {
      typeLabel->setText("<font color=green>Grado </font><font color=blue>A</font>");
      estadoBarra->showMessage(tr("Nivel completado tu grado es A.")); }
  if(percent >= 90 && percent < 100) {
      typeLabel->setText("<font color=green>Grado B</font>");
      estadoBarra->showMessage(tr("Nivel completado tu grado es B.")); }
  if(percent >= 80 && percent < 90) {
      typeLabel->setText("<font color=green>Grado C</font>");
      estadoBarra->showMessage(tr("Nivel completado tu grado es C.")); }
  if(percent >= 70 && percent < 80) {
      typeLabel->setText("<font color=green>Grado D</font>");
      estadoBarra->showMessage(tr("Nivel completado tu grado es D.")); }
  if(percent >= 60 && percent < 70) {
      typeLabel->setText("<font color=green>Grado E</font>");
      estadoBarra->showMessage(tr("Nivel completado tu grado es E.")); }
  if(percent < 60) {
      typeLabel->setText("<font color=green>Grado </font><font color=red>F</font> " + str_percent + " [%]");
      estadoBarra->showMessage(tr("Nivel completado tu grado es F.")); }
  correct = 0;
  str_correct = "0";
  fail = 0;
  str_fail = "0";
  restantes = 0;
  str_restantes = "0";
  seguir = 0;
  nterminado = 1;
  ecuaciones1Button->setEnabled(true);
}

void MainWindow::iniciarDino()
{
  xLabel[1]->setAlignment(Qt::AlignCenter);
  xLabel[2]->setAlignment(Qt::AlignCenter);
  xLabel[3]->setAlignment(Qt::AlignCenter);
  xLabel[4]->setAlignment(Qt::AlignCenter);
  xLabel[5]->setAlignment(Qt::AlignCenter);
  xLabel[6]->setAlignment(Qt::AlignCenter);
  xLabel[7]->setAlignment(Qt::AlignCenter);
  xLabel[8]->setAlignment(Qt::AlignCenter);
  xLabel[9]->setAlignment(Qt::AlignCenter);
  xLabel[10]->setAlignment(Qt::AlignCenter);
  xLabel[11]->setAlignment(Qt::AlignCenter);
  xLabel[12]->setAlignment(Qt::AlignCenter);
  xLabel[13]->setAlignment(Qt::AlignCenter);
  xLabel[14]->setAlignment(Qt::AlignCenter);
  xLabel[15]->setAlignment(Qt::AlignCenter);
  xLabel[16]->setAlignment(Qt::AlignCenter);
  xLabel[17]->setAlignment(Qt::AlignCenter);
  xLabel[18]->setAlignment(Qt::AlignCenter);
  xLabel[19]->setAlignment(Qt::AlignCenter);
  xLabel[20]->setAlignment(Qt::AlignCenter);
  respLabel->setAlignment(Qt::AlignCenter);
  typeLabel->setAlignment(Qt::AlignCenter);
  str_sep = "<font color=darkgrey> | </font>";
  infoLabel->setText("Nivel = <font color=blue>DINOSAURIOS</font>" + str_sep + "Intentos = 0" + str_sep + "Categoria = <font color=green>DINOSAURIOS</font>");
  infoLabel->setAlignment(Qt::AlignCenter);
  failLabel->setText("Restantes = 0" + str_sep + "Aciertos = 0" + str_sep + "Fallas = 0" + str_sep + "Porciento = 0 [%]" + str_sep + "Tiempo = 0 [s]");
  failLabel->setAlignment(Qt::AlignVCenter);
  xButton[0]->setText("0"); 
  xButton[1]->setText("1");
  xButton[2]->setText("2");
  xButton[3]->setText("3");
  xButton[4]->setText("4");
  xButton[5]->setText("5");
  xButton[6]->setText("6");
  xButton[7]->setText("7");
  xButton[8]->setText("8");
  xButton[9]->setText("9");
  backButton->setText("<--");
  cleanButton->setText("&Limpiar");
  ecuaciones1Button->setText("&Ecuaciones");
  ecuaciones2Button->setText("Nada");
  estadisButton->setText("E&stadisticas");
  optionsButton->setText("&Opciones");
  desigualButton->setText("&Desigualdades");
  helpButton->setText("&Ayuda");
  potenciaButton->setText("Nada");
  quitButton->setText("&Cerrar");
  labelsGBox->setStyleSheet("background-color:white;");
  typeLabel->setText("<font color=blue>DINOSAURIOS</font>");
  typerespGBox->setStyleSheet("background-color:skyblue;");
  typeLabel->setAlignment(Qt::AlignRight);
  respLabel->setAlignment(Qt::AlignLeft);
  inicio();
  crearAcciones();
  crearMenu();
  crearEstadoB();
  connect(xButton[0], SIGNAL(clicked()), this, SLOT(print_0()));
  connect(xButton[1], SIGNAL(clicked()), this, SLOT(print_1()));
  connect(xButton[2], SIGNAL(clicked()), this, SLOT(print_2()));
  connect(xButton[3], SIGNAL(clicked()), this, SLOT(print_3()));
  connect(xButton[4], SIGNAL(clicked()), this, SLOT(print_4()));
  connect(xButton[5], SIGNAL(clicked()), this, SLOT(print_5()));
  connect(xButton[6], SIGNAL(clicked()), this, SLOT(print_6()));
  connect(xButton[7], SIGNAL(clicked()), this, SLOT(print_7()));
  connect(xButton[8], SIGNAL(clicked()), this, SLOT(print_8()));
  connect(xButton[9], SIGNAL(clicked()), this, SLOT(print_9()));
  connect(backButton, SIGNAL(clicked()), this, SLOT(backSpace()));
  connect(cleanButton, SIGNAL(clicked()), this, SLOT(cleanEdit()));
  connect(quitButton, SIGNAL(clicked()), this, SLOT(close())); 
  colores();
}

void MainWindow::crearEstadoB()
{
  //tiempoEBLabel = new QLabel();
  //tiempoEBLabel->setText("Tiempo de juego: 00:00:00 [s]");
  //tiempoEBLabel->setAlignment(Qt::AlignRight);
  //estadoBarra->addWidget(tiempoEBLabel, 4);
  //horaEBLabel = new QLabel();
  //horaEBLabel->setText("15:49");
  //horaEBLabel->setAlignment(Qt::AlignRight);
  //estadoBarra->addWidget(horaEBLabel, 3);
  //fechaEBLabel = new QLabel();
  //fechaEBLabel->setText(str_sep + "Jueves 24 de octubre");
  //fechaEBLabel->setAlignment(Qt::AlignRight);
  //estadoBarra->addWidget(fechaEBLabel, 3);
  estadoBarra->showMessage(tr("Bienvenido a DINOSUMA presione enter para iniciar."));
}

void MainWindow::crearAcciones()
{
  act_e1Nivel1 = new QAction(this);
  act_e1Nivel1->setText("+ n1 · n2 + n3 + n4 = " + str_variable);
  connect(act_e1Nivel1, SIGNAL(activated()), this, SLOT(nivel1())); 
  act_e1Nivel2 = new QAction(this);
  act_e1Nivel2->setText("+ n1 · n2 + n3 = " + str_variable); 
  connect(act_e1Nivel2, SIGNAL(activated()), this, SLOT(nivel2()));
  act_e1Nivel3 = new QAction(this);
  act_e1Nivel3->setText("+ n1 + n2 + n3 + n4 = " + str_variable);
  connect(act_e1Nivel3, SIGNAL(activated()), this, SLOT(nivel3()));
  act_e1Nivel4 = new QAction(this);
  act_e1Nivel4->setText("+ n1 · n2 · n3 + n4 = " + str_variable);
  connect(act_e1Nivel4, SIGNAL(activated()), this, SLOT(nivel4()));
  act_e1Nivel5 = new QAction(this);
  act_e1Nivel5->setText("+ n1 · n2 + n3 · n4 + n5 = " + str_variable);
  connect(act_e1Nivel5, SIGNAL(activated()), this, SLOT(nivel5()));
  act_e1Nivel6 = new QAction(this);
  act_e1Nivel6->setText("+ n1 + n2 + n3 + n4 + n5 = " + str_variable);
  connect(act_e1Nivel6, SIGNAL(activated()), this, SLOT(nivel6()));
  act_e1Nivel7 = new QAction(this);
  act_e1Nivel7->setText("+ n1 + n2 + n3 + n4 + n5 = " + str_variable);
  connect(act_e1Nivel7, SIGNAL(activated()), this, SLOT(nivel7()));
  act_e1Nivel8 = new QAction(this);
  act_e1Nivel8->setText("+ n1 + n2 + n3 + n4 + n5 + n6 = " + str_variable);
  connect(act_e1Nivel8, SIGNAL(activated()), this, SLOT(nivel8()));
  act_e1Nivel9 = new QAction(this);
  act_e1Nivel9->setText("+ n1 + n2 + n3 + n4 + n5 + n6 + n7 = " + str_variable);
  connect(act_e1Nivel9, SIGNAL(activated()), this, SLOT(nivel9()));

  act_dNivel1 = new QAction(this);
  act_dNivel1->setText("Nivel 1");
  act_dNivel2 = new QAction(this);
  act_dNivel2->setText("Nivel 2");
  act_dNivel3 = new QAction(this);
  act_dNivel3->setText("Nivel 3");
  act_dNivel4 = new QAction(this);
  act_dNivel4->setText("Nivel 4");
  act_dNivel5 = new QAction(this);
  act_dNivel5->setText("Nivel 5");
  act_dNivel6 = new QAction(this);
  act_dNivel6->setText("Nivel 6");
  act_dNivel7 = new QAction(this);
  act_dNivel7->setText("Nivel 7");
  act_dNivel8 = new QAction(this);
  act_dNivel8->setText("Nivel 8");
  act_dNivel9 = new QAction(this);
  act_dNivel9->setText("Nivel 9");

  act_Guardar = new QAction(this);
  act_Guardar->setText("&Guardar");
  act_guardarSalir = new QAction(this);
  act_guardarSalir->setText("&Guardar y salir");
  act_Salir = new QAction(this);
  act_Salir->setText("&Salir");
  connect(act_Salir, SIGNAL(activated()), this, SLOT(close()));
  //----------------------------------------------------------
  act_Resumen = new QAction(this);
  act_Resumen->setText("&Resumen");
  //--------------------------------------------------------
  act_tamanoFuente = new QAction(this);
  act_tamanoFuente->setText("&Tamaño de la fuente");

  act_tamanoFuenteSigno = new QAction(this);
  act_tamanoFuenteSigno->setText("&Tamaño de la fuente del signo");
  //-------------------------------------------------------	
  act_Acerca = new QAction(this);
  act_Acerca->setText("&Acerca de DINOSUMA...");
  connect(act_Acerca, SIGNAL(activated()), this, SLOT(acerca()));
}

void MainWindow::crearMenu()
{
  menuBarra->addMenu(menu_Iniciar);
  menuBarra->addAction(menu_Iniciar->menuAction());
  menu_Iniciar->addSeparator();
  menu_Iniciar->addAction(act_Guardar);
  menu_Iniciar->addAction(act_guardarSalir);
  menu_Iniciar->addAction(act_Salir);
  menuBarra->addMenu(menu_Ecuaciones1);
  menuBarra->addAction(menu_Ecuaciones1->menuAction());
  menu_Ecuaciones1->addAction(act_e1Nivel1);
  menu_Ecuaciones1->addAction(act_e1Nivel2);
  menu_Ecuaciones1->addAction(act_e1Nivel3);
  menu_Ecuaciones1->addAction(act_e1Nivel4);
  menu_Ecuaciones1->addAction(act_e1Nivel5);
  menu_Ecuaciones1->addAction(act_e1Nivel6);
  menu_Ecuaciones1->addAction(act_e1Nivel7);
  menu_Ecuaciones1->addAction(act_e1Nivel8);
  menu_Ecuaciones1->addAction(act_e1Nivel9);
  menuBarra->addMenu(menu_Desigualdades);
  menuBarra->addAction(menu_Desigualdades->menuAction());
  menu_Desigualdades->addAction(act_dNivel1);
  menu_Desigualdades->addAction(act_dNivel2);
  menu_Desigualdades->addAction(act_dNivel3);
  menu_Desigualdades->addAction(act_dNivel4);
  menu_Desigualdades->addAction(act_dNivel5);
  menu_Desigualdades->addAction(act_dNivel6);
  menu_Desigualdades->addAction(act_dNivel7);
  menu_Desigualdades->addAction(act_dNivel8);
  menu_Desigualdades->addAction(act_dNivel9);
  menuBarra->addMenu(menu_Estadisticas);
  menuBarra->addAction(menu_Estadisticas->menuAction());
  menu_Estadisticas->addAction(act_Resumen);
  menuBarra->addMenu(menu_Opciones);
  menuBarra->addAction(menu_Opciones->menuAction());
  menu_Opciones->addAction(act_tamanoFuente);
  menu_Opciones->addAction(act_tamanoFuenteSigno);
  menuBarra->addMenu(menu_Ayuda);
  menuBarra->addAction(menu_Ayuda->menuAction());
  menu_Ayuda->addAction(act_Acerca);
}

void MainWindow::respuestaTiempo()
{  
  trespuesta++;
  str_trespuesta = QString::number(trespuesta);
  failLabel->setText("Restantes = " + str_restantes + str_sep +"Aciertos = " + str_correct + str_sep + "Fallas = " + str_fail + str_sep + "Porciento = " + str_percent  + " [%]" + str_sep + "Tiempo = " + str_trespuesta + " [s]");
}

void MainWindow::colores()
{
  //color[18] = "blue";
  color[17] = "aqua";
  color[16] = "chartreuse";
  color[15] = "cornflowerblue";
  color[14] = "coral";
  color[13] = "deeppink"; 
  color[12] = "dodgerblue"; 
  color[11] = "fuchsia";
  color[10] = "greenyellow";
  color[9] = "magenta";
  color[8] = "orangered";
  color[7] = "red";
  color[6] = "skyblue";
  color[5] = "tomato";
  color[4] = "violet";
  color[3] = "yellow";
  color[2] = "orange";
  color[1] = "lime";
  color[0] = "gold";
}

void MainWindow::acerca()
{
  AcercaDialogo *dialog = new AcercaDialogo(this);
  dialog->setWindowTitle("Acerca de DINOSUMA");
  dialog->show(); 
  dialog->raise();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
switch(event->key())
  {
    case 48:
      print_N("0");
      break;
    case 49:
      print_N("1");
      break;
    case 50:
      print_N("2");
      break;
    case 51:
      print_N("3");
      break;
    case 52:
      print_N("4");
      break;    
    case 53:
      print_N("5");
      break;
    case 54:
      print_N("6");
      break;
    case 55:
      print_N("7");
      break;
    case 56:
      print_N("8");
      break;
    case 57:
      print_N("9");
      break;
    case 65:
      backSpace();
      break;    
      //case 81:
      //close();
      //break;
    case 67:
      cleanEdit();
    break;
    default:
      QWidget::keyPressEvent(event);
  }
}

void MainWindow::print_0() { print_N("0"); }
void MainWindow::print_1() { print_N("1"); }
void MainWindow::print_2() { print_N("2"); }
void MainWindow::print_3() { print_N("3"); }
void MainWindow::print_4() { print_N("4"); }
void MainWindow::print_5() { print_N("5"); }
void MainWindow::print_6() { print_N("6"); }
void MainWindow::print_7() { print_N("7"); }
void MainWindow::print_8() { print_N("8"); }
void MainWindow::print_9() { print_N("9"); }