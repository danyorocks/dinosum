//Written by Daniel Arroyo <danyorocks@gmail.com>

#include <QtGui/QPushButton>
#include <QtGui/QHBoxLayout>
#include <QtGui/QVBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QGridLayout>
#include <QtGui/QSpacerItem>
#include <QtGui/QDialog>

#include "acercadialogo.h"

AcercaDialogo::AcercaDialogo(QWidget *parent) : QDialog(parent)
{
  noLabel = new QLabel();
  noLabel->setAlignment(Qt::AlignCenter);
  dinosumaLabel = new QLabel();
  dinosumaLabel->setAlignment(Qt::AlignCenter);
  dinoLabel = new QLabel();
  dinoLabel->setAlignment(Qt::AlignCenter);
  decLabel = new QLabel();
  decLabel->setAlignment(Qt::AlignCenter);
  versionLabel = new QLabel();
  versionLabel->setAlignment(Qt::AlignCenter);
  websiteLabel = new QLabel();
  emailLabel = new QLabel();
  nombreLabel = new QLabel();
  copyLabel = new QLabel();
  cerrarButton = new QPushButton();
  cerrarButton->setText("&Cerrar");
  QFont fontdinosuma;
  fontdinosuma.setPointSize(24);
  dinosumaLabel->setFont(fontdinosuma);
  dinosumaLabel->setText("<font color=#ff00ff>DINOSUM</font>");
  versionLabel->setText("version 0.0.0.2");
  websiteLabel->setText("Pagina oficial: ");
  emailLabel->setText("Correo electronico: danyorocks@gmail.com");
  nombreLabel->setText("Programado por: Daniel Arroyo Plata");  
  copyLabel->setText("Copyright (c) 2017 Daniel Arroyo Plata");
  mainVLayout = new QVBoxLayout(this);
  gridILayout = new QGridLayout();
  decLabel->setPixmap(QPixmap("imagenes/Dec.png"));
  gridILayout->addWidget(decLabel, 0, 3, 1, 1);
  gridILayout->addWidget(dinosumaLabel, 0, 1, 1, 1);
  dinoLabel->setPixmap(QPixmap("imagenes/dino.png"));
  gridILayout->addWidget(dinoLabel, 0, 2, 1, 1);
  noLabel->setPixmap(QPixmap("imagenes/N0.png"));
  gridILayout->addWidget(noLabel, 0, 0, 1, 1);
  mainVLayout->addLayout(gridILayout);
  mainVLayout->addWidget(versionLabel);
  mainVLayout->addWidget(websiteLabel);
  mainVLayout->addWidget(emailLabel);
  mainVLayout->addWidget(nombreLabel);
  mainVLayout->addWidget(copyLabel);
  buttonHLayout = new QHBoxLayout();
  horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  buttonHLayout->addItem(horizontalSpacer);
  buttonHLayout->addWidget(cerrarButton);
  mainVLayout->addLayout(buttonHLayout);
  connect(cerrarButton, SIGNAL(clicked()), this, SLOT(close()));
}